// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
if (runIndex !== null) {
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    //var runNames = ["Run A", "Run B"];

	//Getting the list of saved runs from local storage to an array
    var savedRuns = JSON.parse(localStorage.getItem(APP_PREFIX + "-savedRuns"))
    
	//Getting the currently selected runs from that array
	var currentStorageItem = JSON.parse(JSON.stringify(savedRuns[runIndex]));
    
	//Converting the currently selected run to a rub object
	var currentRun = Run.convertStorageItemToRun(currentStorageItem);

   //setting the headerBarTitle
    document.getElementById("headerBarTitle").textContent = currentRun.name;
}

class Page {
    constructor() {
        this.map = new Map(this);
		
    }

    setPage() {
        this.timeSnap.page = this;
    
	}

    setValues(currentRun){

        //Finding the lngLat values of the path taken
		let locations = [currentRun._initial, ...currentRun._landMarks, currentRun._end];
		
		//Initialising distance 
		let distance = 0;


        for(let index = 0; index < locations.length-1; index++){
			
			// Getting the lanLat of initial location
			let lngLat1 = new mapboxgl.LngLat(locations[index].lng, locations[index].lat);
			
			// Getting the lngLat value of the final destination
			let lngLat2 = new mapboxgl.LngLat(locations[index+1].lng, locations[index+1].lat);
			
			//Findinf the distance between the locations
			distance+=lngLat1.distanceTo(lngLat2);
			console.log(distance)
			
			//Displaying initial location
			this.map.addMarker(lngLat1);
			
			//Displaying Destination location
			if(index===(locations.length-1)){
				this.map.addMarker(lngLat2);
			}
        }

		/*
			This function changes the marker colour of the initial and final destination.
		*/
		function change(num, color){
			this.map._markers[num].getElement().style.backgroundColor = color;
		}
		
		//Setting the initial location the user starts the run to blue.
		change.call(this, 0, "blue");
		
		//Setting the final destinatiobn colour to red
		change.call(this, locations.length-2, "red");
		
		//Zooming into the map
		this.map.map.flyTo({
			center:locations[0],
			zoom:15
		});

		//Starting time in miliseconds
		let startMillis = currentRun._times.runStarted;
		
		//Ending time in miliseconds 
		let endMillis = currentRun._times.runCompleted;
		
		//Checking the time taken for the run and converting it seconds
		let duration = (endMillis - startMillis)/1000;
		
		//Calculating the average speed of the run
		let speed = distance/duration;
		
		//Returning the values of the total distance user travelled,duration of the run, and the average speed of the run 
		return [distance, duration, speed];
	}	

	/*
		This Method removes a run that has been done.
	*/
	deleteRun(){
		
		//Gets the index for the run to be deleted
		let selectedRun = +localStorage.getItem(APP_PREFIX + "-selectedRun");
		
		//Gets All the saved runs
		let allRuns = JSON.parse(localStorage.getItem(APP_PREFIX + "-savedRuns"));
		
		//From the all runs array,Removing 1 run starting from the selcted run index
		allRuns.splice(selectedRun, 1);

		//Converted the new list without that run to a string
		let allRunsString = JSON.stringify(allRuns);
		
		//Store that
		localStorage.setItem(APP_PREFIX + "-savedRuns", allRunsString);
		
		//Go to the Main page when delete button is clicked
		location.href="index.html";
	}
}


let page = new Page();
let values = page.setValues(currentRun);

//Displaying the distance of the run
document.getElementById("p6").innerHTML = "Distance of the run: " + values[0] + " m";

//Displaying the duration of the run 
document.getElementById("p7").innerHTML = "Duration of the run: " + values[1] + " s";

//Displaying the average speed 
document.getElementById("p8").innerHTML = "Average speed of the run: " + values[2] + " ms^-1";


