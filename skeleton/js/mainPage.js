// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

function viewRun(runIndex) {
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}

window.onload = function () {
    this.makeUL(JSON.parse(localStorage.getItem(APP_PREFIX + "-savedRuns")));
};

function makeUL(array) {
    // Create the list element:
    var list = document.getElementById('runsList');

    for (var i = 0; i < array.length; i++) {
        // Create the list item:
        var listItem = document.createElement('li');
        
        listItem.setAttribute("class", "mdl-list__item mdl-list__item--two-line")
        
        listItem.setAttribute("onClick", "viewRun(" + i + ");")
        // Set its contents:
        
		//listItem.appendChild(document.createTextNode(array[i]));
        listItem.insertAdjacentHTML('afterbegin', '<span class="mdl-list__item-primary-content"><span>' + array[i]["_name"] + '</span> <span class="mdl-list__item-sub-title">Optional extra info</span></span>');
        
        // Add it to the list:
        list.appendChild(listItem);
    }
}
       