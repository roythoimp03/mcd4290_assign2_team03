// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];

/*
   @property {mapboxgl.LogLat}initialLocation
   @property {Date} timeRunStarted
   @property {mapboxgl.LogLat} destinationLocation
   @property {mapboxgl.LogLat[]} landMarks - through addToLandMarks only
   @property {Date} timeRunEnded
   @property {string} name
*/
class Run {
    constructor() {
        this._name = ""
        this._initial = new mapboxgl.LngLat(0, 0);
        this._destination = new mapboxgl.LngLat(0, 0);
        this._landMarks = [];
        this._times = {
            runStarted: new Date().getTime(),
            runCompleted: new Date().getTime()
        }
    };


    set initialLocation(newStart) {
        this._initial = newStart;
    }

    get initialLocation() {
        return this._initial;
    }

    get timeRunStarted() {
        return new Date(this._times.runStarted);
    }

    set timeRunStarted(newTime) {
        this._times.runStarted = newTime.getTime();
    }

    set destinationLocation(newEnd) {
        this._end = newEnd
    }

    get destinationLocation() {
        return this._end;
    }

    get timeRunEnded() {
        return new Date(this.times.runCompleted);
    }

    set timeRunEnded(newTime) {
        this._times.runCompleted = newTime.getTime();
    }

    set name(newName) {
        this._name = (typeof newName === "string") ? newName : this._name;
    }

    get name() {
        return this._name;
    }

    /*
      Save the run to the currently active run when location changes
    */
    addToLandMarks(newLocation) {
        //Add it to the landMarks
        this._landMarks.push(newLocation);
    }
   
	/*
		This saves the list of runs
	*/
    saveRunToLocalStorage() {        
		let inStore = localStorage.getItem(APP_PREFIX + "-savedRuns");
        if (inStore != null) {
            inStore = JSON.parse(inStore);
            inStore.push(this);
        } else {
            inStore = [];
            inStore.push(this);
        }

        localStorage.setItem(APP_PREFIX + "-savedRuns", JSON.stringify(inStore));  
        }
    
    /*
        This method converts the selected run from local storage to the run object
    */
     static convertStorageItemToRun(storageItem) {
        var tempRun = new Run();
        tempRun.name = storageItem._name;
        tempRun.initialLocation = storageItem._initial;
        tempRun.timeRunStarted = new Date(storageItem._times.runStarted);
        tempRun.destinationLocation = storageItem._end;
        tempRun.timeRunEnded = new Date(storageItem._times.runCompleted);
        storageItem._landMarks.forEach(element => {
            tempRun.addToLandMarks(element);
        });
        return tempRun;
    }
   
}

class Map {

    constructor(page) {
        this._page = page;
        mapboxgl.accessToken = 'pk.eyJ1Ijoic2hhbGFucGVyZXJhIiwiYSI6ImNrOWtoOGcxbjAxdzMzZnJ4MW1wOWl2NG0ifQ.hREeOzgGykuMrebzpL0eQA';
        this.map = new mapboxgl.Map(
            {
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
            }
        );

        //Create and add a controller to control the navigation
        this.map.addControl(new mapboxgl.NavigationControl());

        //Array which stores all the markers
        this._markers = [];

        //Stores the Id of the live marker
        this._idOfLiveMarker = undefined;
    }

    /*
       Adds a marker to a specified location
       @param location - The location of the new marker
    */
    addMarker(location) {
        
        //Create a new marker for the map
        let marker = new mapboxgl.Marker();

        //Set the position of the marker
        marker = marker.setLngLat([location.lng, location.lat]);

        //Add the marker to the map
        marker.addTo(this.map);

        //Add marker to Array
        this._markers.push(marker);

        //Return marker Id
        return this._markers.length - 1;
    }

    /*
    Sets the location of marker identified by markerId to the location specified
       @param {number} markerId
       @param {mapbogl.LngLat} location
    */
    changeMarkerLngLat(markerId, location) {

        //Get the marker
        let marker = this._markers[markerId];

        //Set the position of the marker
        marker = marker.setLngLat([location.lng, location.lat]);

    }
   
    /*
		This method updates the live marker as the user moves around
		@param {mapbogl.LngLat}currentLocation
	*/
    updateLiveMarker(currentLocation) {
        //Check if live marker should be added or updated
        let doesThisHaveAPage = this._page;
        let pageHasMap = doesThisHaveAPage && this._page.map;
        let shouldAddLiveMarker = pageHasMap && typeof (this._idOfLiveMarker) === "undefined";
        let shouldUpdateLiveMarker = !shouldAddLiveMarker && pageHasMap;

        //Add live marker if should
        if (shouldAddLiveMarker) {
            this._idOfLiveMarker = this.addMarker(currentLocation);
        }

        //Update live marker if should
        if (shouldUpdateLiveMarker) {
            this.changeMarkerLngLat(this._idOfLiveMarker, currentLocation);
        }

        //Flys into the current location
        this.flyToLocation(currentLocation);
    }

    /*
		This method,Flys to the location in the map and zoom into location
		@param {mapbogl.LngLat}currentLocation
	*/	
    flyToLocation(currentLocation) {
        this.map.flyTo({
            center: currentLocation,
            zoom: 15
        });
    }
}

class LocationUpdater {

    constructor(page) {

        //Stores the page object
        this._page = page;

        //Stores the current location
        this.currentLocation = undefined;

        //Stores the current velocity
        this.currentVelocity = undefined;

        //Stores the accuracy of the current position
        this.currentAccuracy = undefined;

        //Stores the time at which the current location was updated last
        this.lastTimeUpdate = undefined;

        //PositionOptions
        let positionOptions = {
            enableHighAccuracy: true,
            maximumAge: 100
        };

        //API that gets the lngLat Values
        navigator.geolocation.watchPosition(this.updateLocation.bind(this), this.errorUpdatingLocation.bind(this), positionOptions);
    }

    /*
       Updates the location according to the geoLocation object passed in as a parameter
       @param {Geolocation} geoLocation
    */
    updateLocation(geoLocation) {
        //Store the current location as a lngLat object
        this.currentLocation = new mapboxgl.LngLat(geoLocation.coords.longitude, geoLocation.coords.latitude);

        //Update the time of the last position update
        this.lastTimeUpdate = geoLocation.timestamp;

        //Store current velocity
        this.currentVelocity = {
            speed: geoLocation.coords.speed,
            heading: geoLocation.coords.heading
        };

        //Store the accuracy of the Geolocation reading
        this.currentAccuracy = geoLocation.coords.accuracy;

        //this.currentAccuracy=19;
        if (this._page.timeSnap.currentAccuracy < 20) {
            document.getElementById("new run-button").disabled = false;
        } else {
            displayMessage("Location Inaccurate!")
        }

        let doesThisHaveAPage = this._page;

        //If recording has started
        let doesThatPageHaveRunRecorder = doesThisHaveAPage && this._page.runRecorder;
        let hasRecordingStarted = doesThatPageHaveRunRecorder && this._page.runRecorder.recordingHasStarted;

        this._page.map.updateLiveMarker(this.currentLocation);

        if (hasRecordingStarted) {

            //Notify the run recorder
            this._page.runRecorder.updateWhileRunning(this.currentLocation);

        }

    }

    /*
       Method to call incase geolocation does not work properly
       @param {GeolocationError} geoLocationError
    */
    errorUpdatingLocation(geoLocationError) {
        let message, timeout;
        timeout = 3000;
        switch (geoLocationError.code) {
            case 1:
                message = "Please enable location access for the current page to work properly!";
                break;
            case 2:
                message = "Position data unavailable!";
                break;
            default:
                message = "No position data received!";
        }
        displayMessage(message, timeout);
    }

    set page(page) {
        this._page = page;
    }
}

class LocationClicker {

    constructor(page) {

        //Stores the page object
        this._page = page;

        //Stores the current location
        this.currentLocation = undefined;

        //Stores the current velocity
        this.currentVelocity = undefined;

        //Stores the accuracy of the current position
        this.currentAccuracy = undefined;

        //Stores the time at which the current location was updated last
        this.lastTimeUpdate = undefined;

    }

    updateLocation(event) {
        //Store the current location as a lngLat object
        this.currentLocation = event.lngLat;

        //Update the time of the last position update
        this.lastTimeUpdate = new Date().getTime();

        //Store current velocity
        this.currentVelocity = {
            speed: 0,
            heading: 0
        };

        //Store the accuracy of the Geolocation reading
        this.currentAccuracy = 19;
		
		//The button that generates random destinations are enabled when the accuracy drops below 20
        if (this._page.timeSnap.currentAccuracy < 20) {
            document.getElementById("new run-button").disabled = false;
        }

        let doesThisHaveAPage = this._page;

        //If recording has started
        let doesThatPageHaveRunRecorder = doesThisHaveAPage && this._page.runRecorder;
        let hasRecordingStarted = doesThatPageHaveRunRecorder && this._page.runRecorder.recordingHasStarted;

        this._page.map.updateLiveMarker(this.currentLocation);

        if (hasRecordingStarted) {

            //Notify the run recorder
            this._page.runRecorder.updateWhileRunning(this.currentLocation);

        }

    }

    set page(page) {
        this._page = page;
        page.map.map.on('click', this.updateLocation.bind(this));
    }

}
