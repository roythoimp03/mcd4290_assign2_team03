
class Location {

    constructor(page) {
        this._page = page;
        this.distance = 0;
        this.destination = null;
    }

    /**
      Gets a random location from randomDestination function and adds it to the map
    */
    addRandomLocation() {
        let geoLocation = this._page.timeSnap.currentLocation;

        //Gets a random destination
        let randomLocation = this.randomDestination(geoLocation);

        this._page.runRecorder.run1.addToLandMarks(randomLocation);

        //Mark it on the map
        this._page.map.addMarker(randomLocation);

        //Enable the run start button
        document.getElementById("start run-button").disabled = false;

    }

    /*
       Generates a random destination within 60m - 150m
       @param {mapboxgl.LngLat} startLocation - The start location of the run
    */
    randomDestination(startLocation) {
        let distance = 0;

        //The start location of the run is used as a base to generate a random destination
        let modifiedLocation = startLocation;

        while (distance < 60) {

            //The range of the final destination with relation to the initialLocation
            let range = parseFloat((Math.random() * (0.001000 - 0.000001) + 0.000001).toFixed(6));

            //Adds or substract a range from a value
            function addOrSub(value, range) {
                let addOrSub = Math.floor(Math.random() * 2) + 1;
                return value + ((-1) ** addOrSub) * range;
            }

            //Add or substract range from start longitude and latitude
            let newLng = addOrSub(startLocation.lng, range);
            let newLat = addOrSub(startLocation.lat, range);

            //Generate the new modifiedLocation as end location of run
            var arr = [newLng, newLat];
            modifiedLocation = mapboxgl.LngLat.convert(arr);

            this.destination = modifiedLocation;

            //Get the distance to the modifiedLocation from the start location
            distance = modifiedLocation.distanceTo(startLocation);

        }

        this.distance = distance;
        return modifiedLocation;

    }

    set page(page) {
        this._page = page;
    }
}

class RunRecorder {

    constructor(page) {

        this._page = page;

        //Stores the run's start time
        this.recordStartTime = undefined;

        //Stores the run's end time
        this.recordEndTime = undefined;

        //The init location of the run
        this.initialLocation = undefined;

        //The final location of the run
        this.destinationLocation = undefined;

        //Time now - recordStartTime
        this.timeLapsed = undefined;

        //To know route of run
        this.landMarks = [];

        // Initially, the recording of time has not started. Hence, false.
        this.recordingHasStarted = false;

        //The run which is currently being recorded
        //Instantiate a new run
        this.run1 = new Run();

        //Setting an onclick listener to the start run button
        document.getElementById("start run-button").onclick = this.startRun.bind(this);
    }
	
   
    startRun() {
        //Record the initial location
        this.initialLocation = this._page.timeSnap.currentLocation;
        this.destinationLocation = this._page.location.destination;
        this.recordStartTime = new Date();
        this.timeLapsed = 0;
        this.recordingHasStarted = true;
        this.landMarks.push(this.initialLocation);
        this.updateWhileRunning(this._page.timeSnap.currentLocation);

        //Disabling the New destination button when the Start Run button is clicked
        document.getElementById("new run-button").disabled = true;

        //Disabling the start run button when the start button is clicked so that midway through the run user can't start restart rechording from that position
        document.getElementById("start run-button").disabled = true;

        //Displaying The distance from the current location to the initial Location
        document.getElementById("p2").innerHTML = "The distance from the current location to the initial Location is:" + document.getElementById("p2").innerHTML;

        //Displaying The distance from the current location to the destination Location
        document.getElementById("p3").innerHTML = "The distance from the current location to the destination Location is:" + document.getElementById("p3").innerHTML;



    }
   
    /* 
        Ends new run
    */    
    endRun() {
        this.recordEndTime = new Date();
        this.timeLapsed = 0;
        this.recordingHasStarted = false;
        this.landMarks.push(this.destinationLocation);

        //Displaying the total distance travelled by user
        document.getElementById("p4").innerHTML = "The total distance travelled by the user:" + this.totalDistance(this.landMarks);

        //Display total time taken by user
        document.getElementById("p5").innerHTML = "The total time taken by the user for the run: " + this.timeDifference(this.recordStartTime, this.recordEndTime);

        //Creating save button to save the run
        document.getElementById("save run-button").disabled = false;


    }

    /*
       Funtion to call to update location(among other things) while running
       @param {mapboxgl.LngLat} currentLngLat
    */
    updateWhileRunning(currentLngLat) {
        if (currentLngLat.distanceTo(this._page.location.destination) <= 10) {
            this.endRun();
            console.log("finished");

        } else {
            console.log("still running");
        }
        this.landMarks.push(currentLngLat);
        document.getElementById("s2").innerHTML = currentLngLat.distanceTo(this._page.runRecorder.initialLocation);
        document.getElementById("s3").innerHTML = currentLngLat.distanceTo(this._page.runRecorder.destinationLocation);
    }

    /*
        Calculating the total distance the user took to get to the end destination
    */
    totalDistance(landMarks) {
        var distance = 0;
        for (var i = 0; i < landMarks.length - 1; i++) {
            distance += landMarks[i].distanceTo(landMarks[i + 1]);
        }
        return distance;
    }

    /*
        Calculating the duration the user took for a run
    */
    timeDifference(date1, date2) {
        if (date2 < date1) {
            date2.setDate(date2.getDate() + 1);
        }
        var msec = date2 - date1;
        var hh = Math.floor(msec / 1000 / 60 / 60);
        msec -= hh * 1000 * 60 * 60;
        var mm = Math.floor(msec / 1000 / 60);
        msec -= mm * 1000 * 60;
        var ss = Math.floor(msec / 1000);
        msec -= ss * 1000;
        return hh + "h:" + mm + "m:" + ss + "s";
    }

	/*
	 	Method to call to save a run
	*/
    saveRun() {
        //getting run nickname
        var nickname = document.getElementById("nickname").value;
        var currentRun = new Run();
        currentRun.name = nickname;
        currentRun.initialLocation = this.initialLocation;
        currentRun.timeRunStarted = this.recordStartTime;
        currentRun.destinationLocation = this.destinationLocation;
        currentRun.timeRunEnded = this.recordEndTime;
        this.landMarks.forEach(element => {
            currentRun.addToLandMarks(element);
        });
        currentRun.saveRunToLocalStorage();
        
        //once function over, redirect to:
        location.replace("index.html")
    }

     set page(page) {
        this._page = page;
    }
}

class Page {
    constructor() {
        this.map = new Map(this);
        //this.timeSnap = new LocationClicker(this);
        this.timeSnap = new LocationUpdater(this);
        this.location = new Location(this);
        this.runRecorder = new RunRecorder(this);
    }

    setPage() {
        this.timeSnap.page = this;
        this.location.page = this;
        this.runRecorder.page = this;
    }

    /*
       gets a random location when some button is clicked. WHICH BUTTON?
    */
    buttonClick() {
        //Registers the current location
        page.location.addRandomLocation();
        document.getElementById("p1").innerHTML = "The Estimated(assuming Direct Path is taken)distance betweent the initial Location and destination Location is: " + page.location.distance + "m";
    }

    startRun() {
        page.runRecorder.startRun();
    }

    saveRun() {
        page.runRecorder.saveRun();
    }
}

let page = new Page();
page.setPage();





